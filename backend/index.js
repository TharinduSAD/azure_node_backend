const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const passport = require("passport");
require("./config/passport")(passport);
var cors = require("cors");

const path = require("path");

var app = express();
app.use(helmet());
app.use(cors());

var user_controller = require("./controllers/user_controller");
var offence_controller = require("./controllers/offence_controller");
var fine_controller = require("./controllers/fine_controller");
var driver_controller = require("./controllers/driver_controller");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
  var allowedOrigins = [
    "http://spotweb.azurewebsites.net",
    "http://localhost:3000"
  ];
  var origin = req.headers.origin;
  // Website you wish to allow to
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

// app.use(
//   cors({
//     origin: function(origin, callback) {
//       // allow requests with no origin
//       // (like mobile apps or curl requests)
//       if (!origin) return callback(null, true);
//       if (allowedOrigins.indexOf(origin) === -1) {
//         var msg =
//           "The CORS policy for this site does not " +
//           "allow access from the specified Origin.";
//         return callback(new Error(msg), false);
//       }
//       return callback(null, true);
//     }
//   })
// );

app.listen(3000, function() {
  console.log("Server started at port : 3000");
});

app.use("/user", user_controller);
app.use("/offence", offence_controller);
app.use("/fine", fine_controller);
app.use("/driver", driver_controller);
