var bcrypt = require("bcryptjs");
var sql = require("../db.js");

module.exports.save_admin = (newAdmin, callback) => {
  sql.query("INSERT INTO administrators set ?", newAdmin, callback);
}; // method to save the adinistrator


module.exports.save_user = (user, callback) => {
  console.log(user)
  bcrypt.hash(user.password, 10, (err, hash) => {
    user.password = hash;
    if (err) {
      throw err;
    } else {
      console.log(user.password);

      sql.query("INSERT INTO users set ?", user, callback);
    }
  });
}; // save the user in the database

module.exports.get_user_by_username = (username, callback) => {
  console.log(username);
  sql.query("SELECT * FROM users WHERE username='" + username + "'", callback);
};

//
module.exports.passwordCheck = (plainpassword, hash, callback) => {
  bcrypt.compare(plainpassword, hash, (err, res) => {
    if (err) {
      throw err;
    } else {
      callback(null, res);
    }
  });
};

module.exports.findUserbyId = (id, callback) => {
  sql.query("SELECT * FROM users WHERE user_id ='" + id + "'", callback);
};


module.exports.finduserByIdentityKey = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM administrators WHERE identity_key ='" + identityKey + "'",
    callback
  );
};


module.exports.getAllAdministrators = callback => {
  sql.query("SELECT * FROM users JOIN administrators ON users.identity_key = administrators.identity_key  WHERE type = 'Admin' AND organization = 'Slpolice'", callback);
};

module.exports.getNthAdministrator = (user,callback) => {
  sql.query("SELECT * FROM users JOIN administrators ON users.identity_key = administrators.identity_key  WHERE type = 'Admin' AND organization = 'Slpolice' AND username ='"+user+"'", callback);
};



module.exports.getOfficers = callback =>{
  sql.query("SELECT * FROM users WHERE type = 'Officer' AND organization = 'Slpolice'", callback)
}

module.exports.get_admin_by_identitykey = (identityKey, callback) => {
  sql.query(
    "SELECT * FROM administrators WHERE identity_key ='" + identityKey + "'",
    callback
  );
};

module.exports.get_officer_by_identity_key = (identityKey, callback) =>{
  sql.query(
    "SELECT * FROM users WHERE identity_key ='" + identityKey + "'",
    callback
  );
}

module.exports.change_password = (password, user_id, callback) => {
  bcrypt.hash(password, 10, (err, hash) => {
    password = hash;
    if (err) {
      throw err;
    } else {
      console.log(password);

      sql.query(
        "UPDATE users SET password = '" +
          password +
          "' WHERE user_id = '" +
          user_id +
          "'",
        callback
      );
    }
  });
};

