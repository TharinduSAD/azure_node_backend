const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");
const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Slpolice",
  "slpolice-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object

router.post(
  "/enrollAdmin",
  [
    check("identity")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("first_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("last_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("organization")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("admin_level")
      .exists()
      .isString(),
    check("username")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("password").exists(),
    check("identity_key")
      .exists()
      .isString(),
    check("station")
      .exists()
      .isString()
      .withMessage("Must be a string") //Input Validation
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      User.get_user_by_username(
        //check weather username is already taken or not
        req.body.username,
        async (err, userS, result) => {
          if (err) {
            res.json({ msg: "Error" });
          }
          if (Object.keys(userS).length == 0) {
            console.log("hii");
            try {
              const caInfo =
                ccp.certificateAuthorities["ca.slpolice.trafficfine.com"];
              const caTLSCACerts = caInfo.tlsCACerts;
              const ca = new FabricCAServices(
                caInfo.url,
                { trustedRoots: caTLSCACerts.cacert, verify: false },
                caInfo.caName
              ); //creating the acces to ca cerver

              // Create a new file system based wallet for managing identities.
              const walletPath = path.join(process.cwd(), "wallet");
              const wallet = new FileSystemWallet(walletPath);
              console.log(`Wallet path: ${walletPath}`);

              // Check to see if we've already enrolled the admin user.
              const adminExists = await wallet.exists(req.body.identity);
              if (adminExists) {
                console.log(
                  `An identity for the admin user ${req.body.identity} already exists in the wallet`
                );
                res.status(409).json({
                  state: false,
                  msg: `An identity for the admin user ${req.body.identity} already exists in the wallet`
                });
              } else {
                const enrollment = await ca.enroll({
                  enrollmentID: "admin",
                  enrollmentSecret: "adminpw"
                }); //enroll  the admin
                const identity = X509WalletMixin.createIdentity(
                  "SlpoliceMSP",
                  enrollment.certificate,
                  enrollment.key.toBytes()
                ); //create the identity
                await wallet.import(req.body.identity, identity); //import to the wallet
                console.log(
                  'Successfully enrolled admin user "admin" and imported it into the wallet'
                );

                if (enrollment) {
                  var userC = {
                    username: req.body.username,
                    password: req.body.password,
                    type: "Admin",
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    organization: req.body.organization,
                    user_identity: req.body.identity,
                    identity_key: req.body.identity_key
                  };

                  var admin = {
                    identity_key: req.body.identity_key,
                    level: req.body.admin_level
                  };

                  User.save_user(userC, (err, user) => {
                    //saving the user in external database
                    if (!err) {
                      console.log("Succesfully Added");
                    }
                  });

                  User.save_admin(admin, async (err, user) => {
                    //saving the admin in external database
                    if (err) {
                      throw err;
                    }

                    if (!err) {
                      const userExists = await wallet.exists(req.body.identity);
                      //check weather use as user_identity
                      if (!userExists) {
                        res.status(417).json({
                          state: false,
                          msg: `An identity for the user ${req.body.identity}  does not exist in the wallet`
                        });
                      } else {
                        try {
                          if (req.body.admin_level === "Super") {
                            const gateway = new Gateway(); //Creating the gateway to acces the network
                            await gateway.connect(ccpPath, {
                              wallet,
                              identity: req.body.identity,
                              discovery: { enabled: true, asLocalhost: true }
                            });

                            try{const network = await gateway.getNetwork(
                              //accesing the network
                              "trafficfine"
                            );

                            // Get the contract from the network.
                            const contract = network.getContract("trafficfine");

                            // Submit the specified transaction.

                            
                            await contract.submitTransaction(
                              "addOfficer",
                              userC.identity_key,
                              userC.first_name,
                              userC.last_name,
                              req.body.station
                            );

                            // If admin is super admin user verification is done

                            await contract.submitTransaction(
                              "verifyOfficer",
                              userC.identity_key
                            );

                            res.status(200).json({
                              state: true,
                              msg: "Sucessfully Registered The Administrator"
                            });}catch(err){res.status(500).json({
                              state: false,
                              msg:
                                `Failed with error ${err}`
                            });}

                            
                          } else {
                            User.get_user_by_username(
                              req.body.admin_user,
                              async (err, user) => {
                                console.log(user);
                                const gateway = new Gateway(); //Creating the gateway to acces the network

                                try{
                                  await gateway.connect(ccpPath, {
                                    wallet,
                                    identity: user[0].user_identity,
                                    discovery: {
                                      enabled: true,
                                      asLocalhost: true
                                    }
                                  });
  
                                  const network = await gateway.getNetwork(
                                    "trafficfine"
                                  );
  
                                  // Get the contract from the network.
                                  const contract = network.getContract(
                                    "trafficfine"
                                  );
  
                                  // Submit the specified transaction.
  
                                  await contract.submitTransaction(
                                    "addOfficer",
                                    userC.identity_key,
                                    userC.first_name,
                                    userC.last_name,
                                    req.body.station
                                  );
  
                                  res.status(200).json({
                                    state: true,
                                    msg:
                                      "Sucessfully Registered The Administrator"
                                  });

                                }catch(err){
                                  res.status(500).json({
                                    state: false,
                                    msg:
                                      `Failed with error ${err}`
                                  });
                                }
                                
                              }
                            );
                          }
                        } catch (err) {
                          res.status(500).json({ msg: error });
                        }
                      }
                    } else {
                      res.status(500).json({
                        msg: "something went wrong!!"
                      });
                    }
                  });
                } else {
                  res
                    .status(406)
                    .json({ state: false, msg: "data Is Not Inserted" });
                }
              }
            } catch (err) {
              res.status(403).json({
                state: false,
                msg: `Failed to enroll admin user ${req.body.identity}: ${error}`
              });
            }
          } else {
            res.status(424).json({
              status: false,
              msg:
                "User already exists.Can you please try with another username!"
            });
          }
        }
      );
    }
  }
);

router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.pass;

  User.get_user_by_username(username, async (err, user, result) => {
    //check wether user exists
    if (err) {
      throw err;
    }

    if (!user) {
      res.status(203).json({ state: false, msg: "No user found" });
    }

    if (user) {
      try {
        User.passwordCheck(password, user[0].password, function(err, match) {
          //check wether password is matching or not
          if (err) {
            res
              .status(406)
              .json({ state: false, msg: "your password is incorrect" });
          }

          if (user[0].type == "Admin") {
            User.get_admin_by_identitykey(
              user[0].identity_key,
              (err, userS, result) => {
                const user_sel = {
                  user_id: user[0].user_id,
                  username: user[0].username,
                  type: user[0].type,
                  identity_key: user[0].identity_key,
                  user_level: userS[0].level
                };
                if (match) {
                  const token = jwt.sign(user_sel, secret, {
                    //create jwt token for the authorized user
                    expiresIn: 86400 * 3
                  });
                  res.json({
                    state: true,
                    token: token,
                    user: user_sel
                  });
                } else {
                  res
                    .status(203)
                    .json({ state: false, msg: "password does not match" });
                }
              }
            );
          } else {
            const user_sel = {
              user_id: user[0].user_id,
              username: user[0].username,
              type: user[0].type,
              identity_key: user[0].identity_key
            };
            if (match) {
              const token = jwt.sign(user_sel, secret, {
                //create jwt token for the authorized user
                expiresIn: 86400 * 3
              });
              res.json({
                state: true,
                token: token,
                user: user_sel
              });
            } else {
              res
                .status(203)
                .json({ state: false, msg: "password does not match" });
            }
          }
        });
      } catch (error) {
        res.status(401).json({
          status: false,
          msg: error
        });
      }
    }
  });
});

router.post(
  "/addUser",
  passport.authenticate("jwt", { session: false }),
  [
    check("identity")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("first_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("last_name")
      .exists()
      .isString()
      .isAlpha()
      .withMessage("Must be alphabetical Chars"),
    check("organization")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("username")
      .exists()
      .isString()
      .withMessage("Must be a string"),
    check("password").exists(),
    check("identity_key")
      .exists()
      .isString(),
    check("station")
      .exists()
      .isString()
      .withMessage("Must be a string")
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      User.get_user_by_username(
        req.body.username,
        async (err, user, result) => {
          if (Object.keys(user).length == 0) {
            User.get_user_by_username(
              req.body.admin_user,
              async (err, userS, result) => {
                if (err) {
                  throw err;
                }

                if (Object.keys(userS).length != 0) {
                  const walletPath = path.join(process.cwd(), "wallet");
                  const wallet = new FileSystemWallet(walletPath);
                  console.log(`Wallet path: ${walletPath}`);

                  // Check to see if we've already enrolled the user.
                  const userExists = await wallet.exists(req.body.identity);
                  if (userExists) {
                    console.log(
                      `An identity for the user ${req.body.identity} already exists in the wallet`
                    );
                    res.status(409).json({ msg: "User already exsits" });
                  } else {
                    const adminExists = await wallet.exists(
                      userS[0].user_identity
                    );

                    
                    if (!adminExists) {
                      console.log(
                        `An identity for the admin user ${userS[0].user_identity} does not exist in the wallet`
                      );
                      console.log(
                        "Run the enrollAdmin.js application before retrying"
                      );
                      res.status(409).json({ msg: "Admin is not exsits" });
                    } else {
                      // Create a new gateway for connecting to our peer node.
                      const gateway = new Gateway();
                      await gateway.connect(ccpPath, {
                        wallet,
                        identity: userS[0].user_identity,
                        discovery: { enabled: true, asLocalhost: true }
                      });

                    

                      // Get the CA client object from the gateway for interacting with the CA.
                      const ca = gateway.getClient().getCertificateAuthority();
                      const adminIdentity = gateway.getCurrentIdentity();
                      

                      // Register the user, enroll the user, and import the new identity into the wallet.
                      let secret
                      try{
                        secret = await ca.register(
                          {
                            affiliation: "org2.department1",
                            enrollmentID: req.body.identity,
                            role: "client"
                          },
                          adminIdentity
                        );
                      }catch(err){
                        console.log(err)
                        res
                          .status(500)
                          .json({ msg: `Enrollment is not successfull with error ${err}` });
                      }
                     



                      //creating user identity for the officer

                      let enrollment;
                    
                        if(secret){
                          enrollment = await ca.enroll({
                            enrollmentID: req.body.identity,
                            enrollmentSecret: secret
                          });
                        }
                       

                      if(enrollment){
                        const userIdentity = X509WalletMixin.createIdentity(
                          "SlpoliceMSP",
                          enrollment.certificate,
                          enrollment.key.toBytes()
                        );
                        await wallet.import(req.body.identity, userIdentity);
                        console.log(
                          `Successfully registered and enrolled  user ${req.body.identity} and imported it into the wallet`
                        );
                      }

    
                      if (enrollment) {
                        var user = {
                          username: req.body.username,
                          password: req.body.password,
                          type: "Officer",
                          first_name: req.body.first_name,
                          last_name: req.body.last_name,
                          organization: req.body.organization,
                          user_identity: req.body.identity,
                          identity_key: req.body.identity_key
                        };

                        User.save_user(user, async (err, user) => {
                          if (err) {
                            throw err;
                          }

                          if (user) {
                            try {
                              const walletPath = path.join(
                                process.cwd(),
                                "wallet"
                              );
                              const wallet = new FileSystemWallet(walletPath);
                              const userExists = await wallet.exists(
                                userS[0].user_identity
                              );
                              console.log(userExists);
                              if (!userExists) {
                                res.json({
                                  state: false,
                                  msg: `An identity for the user ${userS[0].user_identity}  does not exist in the wallet`
                                });
                              } else {
                                const gateway = new Gateway(); //Creating the gateway to acces the network
                                await gateway.connect(ccpPath, {
                                  wallet,
                                  identity: userS[0].user_identity,
                                  discovery: {
                                    enabled: true,
                                    asLocalhost: true
                                  }
                                });

                                const network = await gateway.getNetwork(
                                  "trafficfine"
                                );

                                // Get the contract from the network.
                                const contract = network.getContract(
                                  "trafficfine"
                                );

                                // Submit the specified transaction.
                                await contract.submitTransaction(
                                  "addOfficer",
                                  req.body.identity_key,
                                  req.body.first_name,
                                  req.body.last_name,
                                  req.body.station
                                );

                                res.status(200).json({
                                  state: true,
                                  msg: `Successfully added the Officer ${req.body.username}:Reg No ${req.body.identity_key}`
                                });
                              }
                            } catch (err) {
                              res
                                .status(500)
                                .json({ status: false, msg: error });
                            }
                          } else {
                            res
                              .status(500)
                              .json({ msg: "Something went wrong" });
                          }
                        });
                      } else {
                        res
                          .status(500)
                          .json({ msg: "Enrollment is not successfull" });
                      }
                    }
                  }
                } else {
                  res.status(500).json({ msg: "Not a authorised user" });
                }
              }
            );
          }
        }
      );
    }
  }
);

router.post(
  "/verifyofficer",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.get_user_by_username(req.body.user, async (err, user) => {
      if (Object.keys(user).length != 0) {
        try {
          const walletPath = path.join(process.cwd(), "wallet");
          const wallet = new FileSystemWallet(walletPath);
          const gateway = new Gateway(); //Creating the gateway to acces the network
          await gateway.connect(ccpPath, {
            wallet,
            identity: user[0].user_identity,
            discovery: { enabled: true, asLocalhost: true }
          });

          const network = await gateway.getNetwork("trafficfine");

          // Get the contract from the network.
          const contract = network.getContract("trafficfine");

          // Submit the specified transaction.

          await contract.submitTransaction("verifyOfficer", req.body.officer);

          res
            .status(200)
            .json({ msg: `Succesfully verified Officer ${req.body.officer}` });
        } catch (error) {
          res.status(500).json({
            state: false,
            msg: `Failed to verify officer ${req.body.officer}: ${error}`
          });
        }
      } else {
        res.status(500).json({
          state: false,
          msg: `Failed to verify officer you are not authorised to do this`
        });
      }
    });
  }
);

router.get(
  "/administrators",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //get all the administrators from external database
    User.getAllAdministrators((err, users, result) => {
      if (err) {
        throw err;
      }

      if (users) {
        res.status(200).json({
          users: users
        });
      }
    });
  }
);

router.get(
  "/getNthAdministrator/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //get a administrator from external database
    User.getNthAdministrator(req.params.user, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        res.status(200).json({
          user: user
        });
      }
    });
  }
);

router.get(
  "/officers",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //get officers from external database
    User.getOfficers((err, users, result) => {
      if (err) {
        throw err;
      }

      if (users) {
        res.status(200).json({
          users: users
        });
      }
    });
  }
);

router.get(
  "/getAllOfficers/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //get all officers from blockchain
    User.get_user_by_username(req.params.user, async (err, userS, result) => {
      if (err) {
        throw err;
      }
      if (Object.keys(userS).length !== 0) {
        try {
          const walletPath = path.join(process.cwd(), "wallet");
          const wallet = new FileSystemWallet(walletPath);
          console.log(`Wallet path: ${walletPath}`);

          // Check to see if we've already enrolled the user.
          const userExists = await wallet.exists(userS[0].user_identity);
          if (!userExists) {
            res.status(412).json({
              status: false,
              msg: `Identity for user ${req.params.user} does not exits in the wallet`
            });
          } else {
            const gateway = new Gateway(); //Creating the gateway to acces the network
            await gateway.connect(ccpPath, {
              wallet,
              identity: userS[0].user_identity,
              discovery: {
                enabled: true,
                asLocalhost: true
              }
            });

            const network = await gateway.getNetwork("trafficfine");

            // Get the contract from the network.
            const contract = network.getContract("trafficfine");

            const offenceSel = await contract.submitTransaction(
              "getAllOfficers"
            );

            //Creating access data into json objects
            const organizedOffence_sel = JSON.parse(offenceSel.toString());

            res.status(200).json({
              status: true,
              msg: organizedOffence_sel
            });
          }
        } catch (error) {
          res.status(500).json({ status: false, msg: error });
        }
      } else {
        res.status(412).json({ status: false, msg: "Not a registered user" });
      }
    });
  }
);

router.get(
  "/getNthOfficer/:id/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //get nth officcer from blockachain network
    console.log("I am here")
    User.get_user_by_username(req.params.user, async (err, userS, result) => {
      if (err) {
        throw err;
      }
      if (Object.keys(userS).length !== 0) {
        try {
          const walletPath = path.join(process.cwd(), "wallet");
          const wallet = new FileSystemWallet(walletPath);
          console.log(`Wallet path: ${walletPath}`);

          // Check to see if we've already enrolled the user.
          const userExists = await wallet.exists(userS[0].user_identity);
          if (!userExists) {
            res.status(412).json({
              status: false,
              msg: `Identity for user ${req.params.user} does not exits in the wallet`
            });
          } else {
            const gateway = new Gateway(); //Creating the gateway to acces the network
            await gateway.connect(ccpPath, {
              wallet,
              identity: userS[0].user_identity,
              discovery: {
                enabled: true,
                asLocalhost: true
              }
            });

            const network = await gateway.getNetwork("trafficfine");

            // Get the contract from the network.
            const contract = network.getContract("trafficfine");

            const offenceSel = await contract.submitTransaction(
              "selectNthOfficer",
              req.params.id
            );

            const organizedOffence_sel = JSON.parse(offenceSel.toString());

            res.status(200).json({
              status: true,
              msg: organizedOffence_sel
            });
          }
        } catch (error) {
          res.status(500).json({ status: false, msg: error });
        }
      } else {
        res.status(412).json({ status: false, msg: "Not a registered user" });
      }
    });
  }
);

router.post(
  "/getuserbyusername",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //accessing a user by username from external database
    User.get_user_by_username(req.body.username, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        res.status(200).json({
          user: user
        });
      }
    });
  }
);

router.get("/getOfficercounts/:user",passport.authenticate("jwt", { session: false }),(req, res)=>{
  User.get_user_by_username(req.params.user, async (err, userS, result) => {
    if (err) {
      throw err;
    }
    if (Object.keys(userS).length !== 0) {
      try {
        const walletPath = path.join(process.cwd(), "wallet");
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(userS[0].user_identity);
        if (!userExists) {
          res.status(412).json({
            status: false,
            msg: `Identity for user ${req.params.user} does not exits in the wallet`
          });
        } else {
          const gateway = new Gateway(); //Creating the gateway to acces the network
          await gateway.connect(ccpPath, {
            wallet,
            identity: userS[0].user_identity,
            discovery: {
              enabled: true,
              asLocalhost: true
            }
          });

          const network = await gateway.getNetwork("trafficfine");

          // Get the contract from the network.
          const contract = network.getContract("trafficfine");

          const officersSel = await contract.submitTransaction(
            "getAllOfficers"
          );


          //Creating access data into json objects
          const organizedOfficers_sel = JSON.parse(officersSel.toString());

          
          res.status(200).json({officers:Object.keys(organizedOfficers_sel).length})
        }
      } catch (error) {
        res.status(500).json({ status: false, msg: error });
      }
    } else {
      res.status(412).json({ status: false, msg: "Not a registered user" });
    }
  });
})

router.get("/getDrivercounts/:user",passport.authenticate("jwt", { session: false }),(req, res)=>{
  User.get_user_by_username(req.params.user, async (err, userS, result) => {
    if (err) {
      throw err;
    }
    if (Object.keys(userS).length !== 0) {
      try {
        const walletPath = path.join(process.cwd(), "wallet");
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(userS[0].user_identity);
        if (!userExists) {
          res.status(412).json({
            status: false,
            msg: `Identity for user ${req.params.user} does not exits in the wallet`
          });
        } else {
          const gateway = new Gateway(); //Creating the gateway to acces the network
          await gateway.connect(ccpPath, {
            wallet,
            identity: userS[0].user_identity,
            discovery: {
              enabled: true,
              asLocalhost: true
            }
          });

          const network = await gateway.getNetwork("trafficfine");

          // Get the contract from the network.
          const contract = network.getContract("trafficfine");

          const driverSel = await contract.submitTransaction(
            "getAllDrivers"
          );

          //Creating access data into json objects
          const organizedDriver_sel = JSON.parse(driverSel.toString());
          
          res.status(200).json({drivers:Object.keys(organizedDriver_sel).length})
        }
      } catch (error) {
        res.status(500).json({ status: false, msg: error });
      }
    } else {
      res.status(412).json({ status: false, msg: "Not a registered user" });
    }
  });
})


router.post(
  "/changepassword",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user_id = req.body.user;
    const password = req.body.password;
    const prvPassword = req.body.prvPassword;

    console.log(user_id);
    console.log(password);
    console.log(prvPassword);
    User.findUserbyId(user_id, (err, user, result) => {
      if (err) {
        throw err;
      }

      if (user) {
        try {
          User.passwordCheck(prvPassword, user[0].password, function(
            err,
            match
          ) {
            //check wether password is matching or not
            if (err) {
              res
                .status(406)
                .json({ state: false, msg: "your prv Password is incorrect" });
            }

            if (match) {
              User.change_password(password, user_id, (err, user) => {
                if (err) {
                  throw err;
                }

                if (user) {
                  res
                    .status(200)
                    .json({ state: false, msg: "Successfully Updated" });
                }
              });
            } else {
              res
                .status(400)
                .json({ state: false, msg: "password does not match" });
            }
          });
        } catch (error) {
          res.status(400).json({
            status: false,
            msg: error
          });
        }
      } else {
        res.status(400).json({
          state: false,
          msg: "No user found"
        });
      }
    });
  }
);


module.exports = router;
