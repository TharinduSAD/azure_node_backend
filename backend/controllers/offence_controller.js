const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");

const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Slpolice",
  "slpolice-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object

router.post(
  "/addOffence",
  passport.authenticate("jwt", { session: false }),
  [
    check("id")
      .exists()
      .isString(),
    check("title")
      .exists()
      .isString(),
    check("description")
      .exists()
      .isString(),
    check("penaltymarks")
      .isString()
      .exists(),
    check("fine")
      .exists()
      .isString()
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      User.get_user_by_username(req.body.user,async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (!Object.keys(user).length == 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);

            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.body.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              await contract.submitTransaction(
                // submit the transaction
                "addOffence",
                req.body.id,
                req.body.title,
                req.body.description,
                req.body.penaltymarks,
                req.body.fine
              );

              res.status(200).json({
                msg: "successfully added the offence"
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(500).json({  msg: "This is not a authorised user" });
        }
      });
    }
  }
);

router.get(
  "/nthoffence/:id/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    console.log(req.params.user)
    User.get_user_by_username(
      // check weather he is a normal officer
      req.params.user,
      async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(user).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);

            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              const offenceSel = await contract.submitTransaction(
                "selectNthOffence",
                req.params.id
              );

              const organizedOffence_sel = JSON.parse(offenceSel.toString());

              res.status(200).json({
                status: true,
                msg: organizedOffence_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(500).json({ status: false, msg: "Something went wrong" });
        }
      }
    );
  }
);

router.get(
  "/getAllOffences/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.get_user_by_username(
      // check weather he is a normal officer
      req.params.user,
      async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (!Object.keys(user).length == 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);

            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              const offenceSel = await contract.submitTransaction(
                "getAllOffences"
              );

              const organizedOffence_sel = JSON.parse(offenceSel.toString());

              res.status(200).json({
                status: true,
                msg: organizedOffence_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(500).json({ status: false, msg: "This user is not exists" });
        }
      }
    );
  }
);

router.get("/geOffencecounts/:user",passport.authenticate("jwt", { session: false }),(req, res)=>{
  User.get_user_by_username(req.params.user, async (err, userS, result) => {
    if (err) {
      throw err;
    }
    if (Object.keys(userS).length !== 0) {
      try {
        const walletPath = path.join(process.cwd(), "wallet");
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(userS[0].user_identity);
        if (!userExists) {
          res.status(412).json({
            status: false,
            msg: `Identity for user ${req.params.user} does not exits in the wallet`
          });
        } else {
          const gateway = new Gateway(); //Creating the gateway to acces the network
          await gateway.connect(ccpPath, {
            wallet,
            identity: userS[0].user_identity,
            discovery: {
              enabled: true,
              asLocalhost: true
            }
          });

          const network = await gateway.getNetwork("trafficfine");

          // Get the contract from the network.
          const contract = network.getContract("trafficfine");


          const offenceSel = await contract.submitTransaction(
            "getAllOffences"
          )

          //Creating access data into json objects
          const organizedOffence_sel = JSON.parse(offenceSel.toString());
          

          
          res.status(200).json({offences: Object.keys(organizedOffence_sel).length})
        }
      } catch (error) {
        res.status(500).json({ status: false, msg: error });
      }
    } else {
      res.status(412).json({ status: false, msg: "Not a registered user" });
    }
  });
})


module.exports = router;
