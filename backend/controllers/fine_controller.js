const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");

const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Slpolice",
  "slpolice-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object

router.post(
  "/dofine",
  passport.authenticate("jwt", { session: false }),
  [
    check("driver")
      .exists()
      .isString()
      .withMessage("Should be a string"),
    check("offence")
      .exists()
      .isString()
      .withMessage("should be something O1"),
    check("id")
      .exists()
      .isString()
      .withMessage("Something like F1"),
    check("location")
      .exists()
      .isString(),
    check("date")
      .exists()
      .isString(),
    check("time")
      .exists()
      .isString(),
    check("vehicle")
      .exists()
      .isString(),
    check("vehicleNo")
      .exists()
      .isString()
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } else {
      User.get_user_by_username(req.body.user, async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(user).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);

            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.body.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              await contract.submitTransaction(
                // submit the transaction
                "doFine",
                req.body.officer,
                req.body.driver,
                req.body.offence,
                req.body.id,
                req.body.location,
                req.body.date,
                req.body.time,
                req.body.vehicle,
                req.body.vehicleNo,
                req.body.description,
                req.body.expiryDate
              );

              res.status(200).json({
                status: true,
                msg: "Succesfully done the fine"
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      });
    }
  }
);

router.get(
  "/nthofficersFine/:id/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.get_user_by_username(
      req.params.user,
      async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(user).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              const fineSel = await contract.submitTransaction(
                "nthOfficersFines",
                req.params.id
              );

              const organizedFine_sel = JSON.parse(fineSel.toString());

              res.status(200).json({
                status: true,
                msg: organizedFine_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      }
    );
  }
);

router.get(
  "/nthdriversFine/:id/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.get_user_by_username(
      req.params.user,
      async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(user).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);
            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              // Get the contract from the network.
              const contract = network.getContract("trafficfine");

              const fineSel = await contract.submitTransaction(
                "nthDriversFines",
                req.params.id
              );

              const organizedFine_sel = JSON.parse(fineSel.toString());

              res.status(200).json({
                status: true,
                msg: organizedFine_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      }
    );
  }
);

router.get(
  "/nthfine/:id/:user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.get_user_by_username(
      req.params.user,
      async (err, user, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(user).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);

            const userExists = await wallet.exists(user[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: user[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });

              const network = await gateway.getNetwork("trafficfine");

              const contract = network.getContract("trafficfine");

              const offenceSel = await contract.submitTransaction(
                "selectNthFine",
                req.params.id
              );

              res.status(200).json({
                status: true,
                msg: offenceSel.toString()
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        }else{
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      }
    );
  }
);

router.get("/getFinecounts/:user",passport.authenticate("jwt", { session: false }),(req, res)=>{
  User.get_user_by_username(req.params.user, async (err, userS, result) => {
    if (err) {
      throw err;
    }
    if (Object.keys(userS).length !== 0) {
      try {
        const walletPath = path.join(process.cwd(), "wallet");
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists(userS[0].user_identity);
        if (!userExists) {
          res.status(412).json({
            status: false,
            msg: `Identity for user ${req.params.user} does not exits in the wallet`
          });
        } else {
          const gateway = new Gateway(); //Creating the gateway to acces the network
          await gateway.connect(ccpPath, {
            wallet,
            identity: userS[0].user_identity,
            discovery: {
              enabled: true,
              asLocalhost: true
            }
          });

          const network = await gateway.getNetwork("trafficfine");

          // Get the contract from the network.
          const contract = network.getContract("trafficfine");


          const fineSel = await contract.submitTransaction(
            "getAllFines"
          )

          //Creating access data into json objects
          const organizedFine_sel = JSON.parse(fineSel.toString());
          

          
          res.status(200).json({fines: Object.keys(organizedFine_sel).length})
        }
      } catch (error) {
        res.status(500).json({ status: false, msg: error });
      }
    } else {
      res.status(412).json({ status: false, msg: "Not a registered user" });
    }
  });
})

module.exports = router;
