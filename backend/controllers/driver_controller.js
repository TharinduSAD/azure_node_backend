const express = require("express");
var router = express.Router();

var jwt = require("jsonwebtoken");
const secret = "jobmextuv2345";
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

var User = require("../models/user");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");
const ccpPath = path.resolve(
  __dirname,
  "..",
  "tmp",
  "client",
  "Slpolice",
  "slpolice-trafficfine-network.json"
); // The file path to network configeration file
const ccpJSON = fs.readFileSync(ccpPath, "utf8"); // read the file
const ccp = JSON.parse(ccpJSON); // convert it into a javscript object



router.get(
    "/getAllDrivers/:user",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      //get all officers from blockchain
      User.get_user_by_username(req.params.user, async (err, userS, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(userS).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);
  
            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(userS[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: userS[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });
  
              const network = await gateway.getNetwork("trafficfine");
  
              // Get the contract from the network.
              const contract = network.getContract("trafficfine");
  
              const driverSel = await contract.submitTransaction(
                "getAllDrivers"
              );
  
              //Creating access data into json objects
              const organizedDriver_sel = JSON.parse(driverSel.toString());
  
              res.status(200).json({
                status: true,
                msg: organizedDriver_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        } else {
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      });
    }
  );

  router.get(
    "/getNthDriver/:id/:user",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      //get nth officcer from blockachain network
      User.get_user_by_username(req.params.user, async (err, userS, result) => {
        if (err) {
          throw err;
        }
        if (Object.keys(userS).length !== 0) {
          try {
            const walletPath = path.join(process.cwd(), "wallet");
            const wallet = new FileSystemWallet(walletPath);
            console.log(`Wallet path: ${walletPath}`);
  
            // Check to see if we've already enrolled the user.
            const userExists = await wallet.exists(userS[0].user_identity);
            if (!userExists) {
              res.status(412).json({
                status: false,
                msg: `Identity for user ${req.params.user} does not exits in the wallet`
              });
            } else {
              const gateway = new Gateway(); //Creating the gateway to acces the network
              await gateway.connect(ccpPath, {
                wallet,
                identity: userS[0].user_identity,
                discovery: {
                  enabled: true,
                  asLocalhost: true
                }
              });
  
              const network = await gateway.getNetwork("trafficfine");
  
              // Get the contract from the network.
              const contract = network.getContract("trafficfine");
  
              const driverSel = await contract.submitTransaction(
                "selectNthDriver",
                req.params.id
              );
  
              const organizedDriver_sel = JSON.parse(driverSel.toString());
  
              res.status(200).json({
                status: true,
                msg: organizedDriver_sel
              });
            }
          } catch (error) {
            res.status(500).json({ status: false, msg: error });
          }
        } else {
          res.status(412).json({ status: false, msg: "Not a registered user" });
        }
      });
    }
  );

  module.exports = router;
  